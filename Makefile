rust-version:
	@echo "Rust command-line utility versions:"
	rustc --version 			#rust compiler
	cargo --version 			#rust package manager
	rustfmt --version			#rust code formatter
	rustup --version			#rust toolchain manager
	clippy-driver --version		#rust linter

format:
	cargo fmt --quiet

lint:
	cargo clippy --quiet

test:
	cargo test --quiet

watch:
	cargo lambda watch -p 8080 -a 127.0.0.1

build:
	cargo lambda build --release 

deploy:
	cargo lambda deploy --region us-east-1

run:
	cargo run

release:
	cargo build --release

all: format lint test run