# AWS Lambda miniproject

The project is an AWS Lambda function as an HTTP entry point. It receives the integer parameter `number` and states in a message if the number is prime or not.

## Usage
The functionality of the function can be tested by the [API endpoint](https://wwc5dw5yoh.execute-api.us-west-1.amazonaws.com/default/is_prime?number=57). Change the parameter in the link if you want to check other numbers.

### Example
![](images/example.png)

### Function overview
![](images/function_overview.png)

### Testing locally
You can test the function locally by running `make watch` and following the [link](http://127.0.0.1:8080/?number=57).

### Cloudwatch logs of recent invocations
![](images/recent_invocations.png)