use tracing_subscriber::filter::{EnvFilter, LevelFilter};
use lambda_http::{run, service_fn, Body, Error, Request, RequestExt, Response};


async fn function_handler(event: Request) -> Result<Response<Body>, Error> {
    let number_query = event
        .query_string_parameters_ref()
        .and_then(|params| params.first("number"));

	match number_query {
		Some(num_str) => {
			match num_str.parse::<i64>() {
				Ok(num) if num > 0 => {
					let message: String;
					if is_prime::is_prime(num_str) {
						message = format!("The number {num_str} is prime");
					} else {
						message = format!("The number {num_str} is not prime");
					}
					Ok(Response::builder()
						.status(200)
						.header("Content-Type", "text/plain")
						.body(Body::Text(message))
						.expect("Failed to render response"))
				},
				Ok(_) => {
					let message = "Please provide a positive integer (a prime number is bigger than 1).";
					Ok(Response::builder()
						.status(400)
						.header("Content-Type", "text/plain")
						.body(Body::Text(message.into()))
						.expect("Failed to render response"))
				},
				Err(_) => {
					let message = "Please provide a valid integer for the 'number' query parameter.";
					Ok(Response::builder()
						.status(400)
						.header("Content-Type", "text/plain")
						.body(Body::Text(message.into()))
						.expect("Failed to render response"))
				}
			}
		},
		None => {
			let message = "Missing 'number' query parameter. Please provide an integer.";
			Ok(Response::builder()
				.status(400)
				.header("Content-Type", "text/plain")
				.body(Body::Text(message.into()))
				.expect("Failed to render response"))
		}
	}
	
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    tracing_subscriber::fmt()
        .with_env_filter(
            EnvFilter::builder()
                .with_default_directive(LevelFilter::INFO.into())
                .from_env_lossy(),
        )
        .with_target(false)
        .without_time()
        .init();

    run(service_fn(function_handler)).await
}